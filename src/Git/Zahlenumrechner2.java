package Git;

import java.util.Scanner;

public class Zahlenumrechner2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("(1) Umrechnen Dezimalzahl nach Bin\u00e4rzahl");
		System.out.println("(2) Umrechnen Bin\u00e4rzahl nach Dezimalzahl");
//		System.out.println("(3) Umrechnen Dezimalzahl nach Hexadezimalzahl");
//		System.out.println("(4) Umrechnen Hexadezimalzahl nach Dezimalzahl");
		int menuezahl;
		menuezahl = sc.nextInt();

		if (menuezahl == 1) {
			System.out.println("Bitte Dezimalzahl zum Umrechnen eingeben");
			int dezimalzahl;
			dezimalzahl =sc.nextInt();
			System.out.println(dezimalNachBinaer(dezimalzahl));
			}
		if (menuezahl == 2) {
			System.out.println("Bitte Bin\u00e4rzahl zum Umrechnen eingeben");
			String binaerzahl;
			binaerzahl =sc.next();
			System.out.println(binaernachDezimal(binaerzahl));
		}
//		if (menuezahl == 3) {
//			System.out.println("Bitte Dezimalzahl zum Umrechnen eingeben");
//			int dezimalzahl;
//			dezimalzahl =sc.nextInt();
//			System.out.println(dezimalNachHexadezimal(dezimalzahl));
//		}
//		if (menuezahl == 4) {
//			System.out.println("Bitte Hexadezimalzahl zum Umrechnen eingeben");
//			String hexazahl;
//			hexazahl =sc.next();
//			System.out.println(hexadezimalnachDezimal(hexazahl));
//		}

		sc.close();
	}

	

	private static int binaernachDezimal(String binaer) {
		int binaerstellen = binaer.length();
		int dezimal = 0;
		int e = 0;
		for (int i = binaerstellen - 1; i >= 0; i--) {

			char stelle = binaer.charAt(i);
			String ein = "1";
			char eins = ein.charAt(0);
			if (stelle == eins) {
				int g = (int) Math.pow(2, e);
				dezimal += g;

			}
			e++;
		}
		return dezimal;
	}

	

	private static String dezimalNachBinaer(int dezimal) {
		int rest = 0;
		String binaerzahl = "";
		String ergebnis = null;
		boolean ende = false;
		do {
			if (dezimal == 1) {
				ergebnis = "1";
				binaerzahl = ergebnis + binaerzahl;
				ende = true;
			} else {
				rest = dezimal % 2;
				dezimal = dezimal / 2;
				if (rest == 0) {
					ergebnis = "0";
					binaerzahl = ergebnis + binaerzahl;
				} else {
					ergebnis = "1";
					binaerzahl = ergebnis + binaerzahl;
				}
			}

		} while (ende == false);

		return binaerzahl;

	}
	
	private static String dezimalNachHexadezimal(int dezimal) {
		int rest = 0;
		String hexadezimalzahl = "";
		boolean ende = false;
		do {
			if (dezimal == 0)
				break;
			String ergebnis = "";
			rest = dezimal % 16;
			if (dezimal < 10) {
				ergebnis = ergebnis + rest;
				hexadezimalzahl = ergebnis + hexadezimalzahl;
				ende = true;
			} else {

				dezimal = dezimal / 16;
				if (rest < 10) {
					ergebnis = ergebnis + rest;
					hexadezimalzahl = ergebnis + hexadezimalzahl;
				} else {
					if (rest == 10) {
						ergebnis = "a";
						hexadezimalzahl = ergebnis + hexadezimalzahl;
					}
					if (rest == 11) {
						ergebnis = "b";
						hexadezimalzahl = ergebnis + hexadezimalzahl;
					}
					if (rest == 12) {
						ergebnis = "c";
						hexadezimalzahl = ergebnis + hexadezimalzahl;
					}
					if (rest == 13) {
						ergebnis = "d";
						hexadezimalzahl = ergebnis + hexadezimalzahl;
					}
					if (rest == 14) {
						ergebnis = "e";
						hexadezimalzahl = ergebnis + hexadezimalzahl;
					}
					if (rest == 15) {
						ergebnis = "f";
						hexadezimalzahl = ergebnis + hexadezimalzahl;
					}
				}
			}
		} while (ende == false);
		return hexadezimalzahl;

	}

}
